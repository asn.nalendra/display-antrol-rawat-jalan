This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

1. Clone Project

```
git clone https://gitlab.com/yafiabiyyu/display-antrol-rawat-jalan.git antrol-rj

// masuk kedalam directory antrol-rj
cd antrol-rj
```

2. Install package
```
npm install
```

3. Environtment Setup

```
// Buat file .env pada root project
touch .env

// Buat varibale baru dengan nama
BASE_URI = "API_ENDPOINT_MAS_ONY"
```

4. Jalankan Applikasi

```
npm run dev

atau

npm run build && npm run start
```
