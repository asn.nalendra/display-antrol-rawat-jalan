const baseUri = process.env.BASE_URI;

export async function getAvailableDoctor() {
    let tmpDokter = [];
    let listDokter = [];
    let date = (new Date()).toISOString().split("T")[0];

    const response = await fetch(`${baseUri}/ws-bpjs/ref/dokter`, { cache: "no-store" });
    const data = await response.json();

    if (data.metadata.message == "Ok") {
        for (let dokter of data.response) {
            listDokter.push({
                kode_dokter: dokter.kodedokter,
                nama_dokter: dokter.namadokter
            })
        }
    }
    return listDokter;

}

export async function getTotalPasienRs(){
    let date = (new Date()).toISOString().split("T")[0];
    const response = await fetch(`${baseUri}/ws-bpjs/antrean/poli?tanggalperiksa=${date}`)
    const data = (await response.json()).response

    return data.total;
}

async function getMultiplePage(kode_dokter, total_page) {
    const listAntrean = [];
    const date = (new Date()).toISOString().split("T")[0];
    for (let i = 1; i <= total_page; i++) {
        const response = await fetch(`${baseUri}/ws-bpjs/antrean/poli?kodedokter=${kode_dokter}&tanggalperiksa=${date}&page=${i}`, { cache: "no-store" });
        const data = (await response.json()).response;
        listAntrean.push(...data.data);
    }
    return getPasien(listAntrean);
}

async function getByKodeBooking(antrean) {
    const listAntrean = [];
    for (const dataAntrean of antrean) {
        const response = await fetch(`${baseUri}/ws-bpjs/antrean/pendaftaran/kodebooking/${dataAntrean.kodebooking}`, { cache: "no-store" });
        const responseData = await response.json();

        if (responseData.response && responseData.response[0]) {
            const data = responseData.response[0];
            if (data.nik === undefined || data.status === undefined) {
                continue;
            } else {
                dataAntrean.nik = data.nik;
                dataAntrean.status = data.status;
                listAntrean.push(dataAntrean);
            }
        } else {
            continue;
        }
    }
    return listAntrean;
}

async function getPasien(antrean) {
    const listAntrean = [];
    const dataAntrean = await getByKodeBooking(antrean);
    for (const pasien of dataAntrean) {
        const response = await fetch(`${baseUri}/homecare/pasien/detail?nik=${pasien.nik}`, { cache: "no-store" });
        const data = await response.json();

        if (data.success === false) {
            listAntrean.push(await getByNoRM(pasien));
        } else {
            listAntrean.push({
                nomor_antrian: pasien.nomorantrean,
                nama_pasien: data.data.nama,
                alamat_pasien: data.data.Alamat,
                status: pasien.status,
            });
        }
    }
    return listAntrean;
}

export async function getAntrean(dokter){
    const listAntrean = []
    let tmpListAntrean = []
    const date = (new Date()).toISOString().split("T")[0];
    for(let antrean of dokter){
        const response = await fetch(`${baseUri}/ws-bpjs/antrean/poli?kodedokter=${antrean}&tanggalperiksa=${date}`, { cache: "no-store" });
        const data = (await response.json()).response
        
        if(data.total == 0) {
            continue;
        }else{
            if(data.last_page > 1){
                tmpListAntrean = await getMultiplePage(antrean, data.last_page);
            }else{
                tmpListAntrean = await getPasien(data.data);
            }
            listAntrean.push({
                nama_dokter: data.data[0].namadokter,
                nama_poli:data.data[0].namapoli,
                total:data.total,
                antrean:tmpListAntrean
            })
        }
    }
    return listAntrean;
}