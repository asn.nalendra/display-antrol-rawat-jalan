import { Inter } from 'next/font/google'
import { Providers } from './providers'
import { Headers } from '@/component/Headers'

const inter = Inter({ subsets: ['latin'] })

export const metadata = {
	title: 'Antrian (Online) Rawat Jalan',
	description: 'Rumah Sakit Sumberglagah',
}

export default function RootLayout({ children }) {
	return (
		<html lang="en">
			<body className={inter.className}>
				<Providers>
					<Headers/>
					{children}
				</Providers>
			</body>
		</html>
	)
}
