import { getAntrean, getTotalPasienRs } from "@/utils/api";
import { DisplayAntrean } from "@/component/DisplayAntrean";

export default async function Antrean({params}){
    const antrean = await getAntrean(params.slug);
    const totalPasienRs = await getTotalPasienRs();
    return (
        <DisplayAntrean data={antrean} totalRs={totalPasienRs}/>
    )
}