'use client'

import { CacheProvider } from "@chakra-ui/next-js";
import { ChakraProvider } from "@chakra-ui/react";
import { extendTheme } from "@chakra-ui/react";

// Set initial color mode to light
const configTheme = {
    initialColorMode: 'ligth',
    useSystemColorMode: false
}

const theme = extendTheme({configTheme});

export function Providers({children}) {
    return (
        <ChakraProvider>
            <ChakraProvider>
                {children}
            </ChakraProvider>
        </ChakraProvider>
    )
}