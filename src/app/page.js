import { SetupForms } from "@/component/SetupForms"
import { Container } from "@/utils/clientComponent";
import { getAvailableDoctor } from "@/utils/api";

export default async function Home() {
	const dokter = await getAvailableDoctor();
	return (
		<Container maxW="container.xl">
			<SetupForms antrean={dokter}/>
		</Container>
	)
}
