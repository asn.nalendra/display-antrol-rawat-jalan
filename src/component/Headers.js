'use client'

import { Link } from '@chakra-ui/next-js';
import { Box, Container, Flex, Heading } from '../utils/clientComponent';

export function Headers(){
    return (
        <Box borderBottom="1px" borderBottomColor="chakra-border-color" bg="cyan.600">
            <Container maxW="container.xl">
                <Flex align="center" justify="space-between">
                    <Heading ml={7} mt={3} mb={3} color="white" size="md" as="bold"><Link href="/" _hover={{ textDecoration: "none" }}>RSUD</Link></Heading>
                    <Heading mr={7} mt={3} mb={3} color="white" size="md" as="bold">Antrian (Online) Rawat Jalan</Heading>
                </Flex>
            </Container>
        </Box>
    )
}