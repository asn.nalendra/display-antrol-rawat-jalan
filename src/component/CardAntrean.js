import { Card, CardHeader, CardBody, Text, Center } from '@/utils/clientComponent'

export function CardAntrean({title, data}) {
    return (
        <Card maxW="auto" mt={5} size="xl">
            <CardHeader bg="cyan.600" borderTopRadius="lg">
                <Center>
                    <Text size="xs" as="b" color="white" >{title}</Text>
                </Center>
            </CardHeader>
            <CardBody maxH="auto" maxW="auto">
                <Center>
                    {data ? 
                    <Text fontSize="4xl" as="b">{data}</Text>:
                    <Text fontSize="4xl" as="b">0</Text>}
                </Center>
            </CardBody>
        </Card>
    )
}