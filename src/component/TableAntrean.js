'use client'

import { useEffect, useRef } from 'react';
import {
    Table,
    Thead,
    Tbody,
    Tr,
    Th,
    Td,
    TableContainer,
    Center,
    Text
} from '@/utils/clientComponent'

export function TableAntrean({ antrean }) {
    const tableRef = useRef(null);
    useEffect(()=>{
        let tableContainer = tableRef.current;
        let tableHeight = tableContainer.scrollHeight;
        let distanceToScroll = tableHeight - tableContainer.clientHeight;

        let scrollTable = () => {
            if(tableContainer.scrollTop >= distanceToScroll){
                tableContainer.scrollTop = 0;
            }else{
                tableContainer.scrollTop += 1;
            }
        }
        const interval = setInterval(scrollTable, 60);
        return ()=> {
            clearInterval(interval);
        }
    },[antrean])

    return (
        <>
            <TableContainer mt={10} mb={4} maxWidth="100%" h="315px" ref={tableRef} mr={5} ml={5}>
                <Table h="315">
                    <Thead position="sticky" top={0} bg="white">
                        <Tr>
                            <Th><Center><Text fontSize="xl" as="b" color="black">NO. ANTRIAN</Text></Center></Th>
                            <Th><Text fontSize="xl" as="b" color="black">NAMA</Text></Th>
                            <Th><Text fontSize="xl" as="b" color="black">ALAMAT</Text></Th>
                            <Th><Center><Text fontSize="xl" as="b" color="black">STATUS</Text></Center></Th>
                        </Tr>
                    </Thead>
                    <Tbody>
                        {antrean.map((item, index) => (
                            <Tr key={index}>
                                <Td><Center><Text fontSize="lg" as="b">{item.nomor_antrian}</Text></Center></Td>
                                <Td><Text fontSize="lg" as="b">{item.nama_pasien}</Text></Td>
                                <Td><Text fontSize="lg" as="b">{item.alamat_pasien}</Text></Td>
                                <Td><Center><Text fontSize="lg" as="b">{item.status}</Text></Center></Td>
                            </Tr>
                        ))}
                    </Tbody>
                </Table>
            </TableContainer>
        </>
    )
}