'use client'

import { useEffect, useState } from "react"
import { useRouter } from "next/navigation";


// Import Component
import { Grid, GridItem } from "../utils/clientComponent";
import { HeadersAntrean } from "@/component/HeadersAntrean"
import { TableAntrean } from "@/component/TableAntrean";
import { CardAntrean } from "./CardAntrean";

export function DisplayAntrean({data, totalRs}) {
    const router = useRouter();
    const [index, setIndex] = useState(0);
    useEffect(() => {

        const intervalId = setInterval(() => {
            if (index < data.length - 1) {
                setIndex(index + 1);
            } else {
                setIndex(0);
                router.refresh();
            }
        }, 20000);
        return () => clearInterval(intervalId);
    }, [index, data])

    const currentData = data[index];
    const dokter = currentData.nama_dokter;
    const namaPoli = currentData.nama_poli;
    const total_antrean = currentData.total;
    const antrean = currentData.antrean
    
    return(
        <>
            <HeadersAntrean dokter={dokter} poli={namaPoli}/>
            <TableAntrean antrean={antrean}/>
            <Grid 
				templateColumns="repeat(8,1fr)" gap={4}>
				<GridItem colSpan={2} colStart={3}>
					<CardAntrean title="Jumlah Pasien Poli" data={total_antrean}/>
				</GridItem>
				<GridItem colSpan={2}>
					<CardAntrean title="Jumlah Pasien RS" data={totalRs}/>
				</GridItem>
			</Grid>
        </>
    )
}