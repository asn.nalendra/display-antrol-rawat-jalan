'use client'
import { Link } from "@chakra-ui/next-js";
import {
    Button,
    FormControl,
    Flex,
    Heading,
    Select,
    Stack,
    HStack,
    Text,

} from "@/utils/clientComponent";
import { ArrowForwardIcon } from "@chakra-ui/icons";
import { useState } from "react";

export function SetupForms({ antrean }) {
    const [selectedData, setSelectedData] = useState([]);
    const [selectedOption, setSelectedOption] = useState('');

    const handleSelectChange = (event) => {
        setSelectedOption(event.target.value);
    }

    const handleTambahClick = () => {
        setSelectedData([...selectedData, selectedOption])
        setSelectedOption('')
        console.log(selectedData)
    }

    const handleOpenAntrean = () => {
        console.log(selectedData);
    }

    const handeResetClick = () => {
        setSelectedData([])
    }
    return (
        <>
            <Flex
                minH={'88vh'}
                align={'center'}
                justify={'center'}>
                <Stack
                    spacing={4}
                    w={'full'}
                    maxW={'md'}
                    rounded={'xl'}
                    boxShadow={'lg'}
                    p={6}
                    my={12}>
                    <Heading lineHeight={1.1} fontSize={{ base: '2xl', md: '3xl' }}>
                        Display Setting
                    </Heading>
                    <Text
                        fontSize={{ base: 'sm', sm: 'md' }}>
                        Pilih Dokter untuk di tampilkan kedalam display
                    </Text>
                    <FormControl id="dokter">
                        <Select placeholder="Pilih Dokter" value={selectedOption} onChange={handleSelectChange}>
                            {antrean.map((item, index) => (
                                <option
                                    key={item.kode_dokter}
                                    value={item.kode_dokter}
                                    disabled={selectedData.some(selectedItem => selectedItem === item.kode_dokter)}
                                >{item.nama_dokter}</option>
                            ))}
                        </Select>
                    </FormControl>
                    <HStack spacing={3}>
                        <Button
                            w={'50%'}
                            bg={'blue.400'}
                            color={'white'}
                            _hover={{
                                bg: 'blue.500',
                            }}
                            onClick={handleTambahClick}>
                            Pilih Dokter
                        </Button>
                        <Button
                            w={'50%'}
                            bg={'red.400'}
                            color={'white'}
                            _hover={{
                                bg: 'red.500',
                            }}
                            onClick={handeResetClick}>
                            Hapus Pilihan
                        </Button>
                        {selectedData.length > 0 ? (
                            <Button
                                w={'60%'}
                                bg={'green.400'}
                                color={'white'}
                                _hover={{
                                    bg: 'green.500',
                                }}
                                rightIcon={<ArrowForwardIcon />}
                            >
                                <Link href={`/antrean/${selectedData.join('/')}`} _hover={{ textDecoration: "none" }}>Buka Antrean</Link>
                            </Button>
                        ) : (
                            <Button
                                w={'60%'}
                                bg={'gray.400'}
                                color={'white'}
                                _hover={{
                                    bg: 'gray.500',
                                }}
                                isDisabled
                                rightIcon={<ArrowForwardIcon />}
                            >
                                Buka Antrean
                            </Button>
                        )}
                    </HStack>
                </Stack>
            </Flex>

        </>
    )
}