'use client'

import { Heading, VStack, Center } from "@/utils/clientComponent"

export function HeadersAntrean({ dokter, poli }) {
    return (
        <Center>
            <VStack spacing={2} mt={10}>
                <Heading fontStyle="bold" size="lg">{poli}</Heading>
                <Heading fontStyle="bold" size="lg">{dokter}</Heading>
            </VStack>
        </Center>
    )
}